/*
  Time Lapse
  Press Shutter for 1 second, wait for 5 minutes
 
  This code is in the public domain.
 */

int led=13; 
int shutter=7;

int led5=12;
int led4=11;
int led3=10;
int led2=9;
int led1=8;
// Maximum 10 minutes
double tempsMax = 10 * 60 ;
// Minimum 1 s
double tempsMin = 1 ;
double msec=5;

void setup() {                
  // initialize the digital pin as an output.
  pinMode(led, OUTPUT);
  pinMode(shutter, OUTPUT);     
  
  pinMode(led5, OUTPUT);
  pinMode(led4, OUTPUT);
  pinMode(led3, OUTPUT);
  pinMode(led2, OUTPUT);
  pinMode(led1, OUTPUT);
  Serial.begin(9600);
  
}

double calculDelay()
{
  int sensorValue = analogRead(0);
  double tempo = 0;
  tempo = (((tempsMax - tempsMin) *  sensorValue) / 1024);
  tempo += tempsMin;
  Serial.println( tempo );
  tempo *= 1000;

  tempo /= 5;
  return tempo;
}

void loop() {
  
  msec = calculDelay();

  digitalWrite(shutter, LOW);    // release shutter
  digitalWrite(led, LOW);        // light off
  
  digitalWrite( led5, HIGH);
  digitalWrite( led4, LOW);
  digitalWrite( led3, LOW);
  digitalWrite( led2, LOW);
  digitalWrite( led1, LOW);  
  
  msec = calculDelay();
  delay(msec);              // wait 1 minutes
  digitalWrite( led5, LOW);
  digitalWrite( led4, HIGH);

  msec = calculDelay();
  delay(msec);              // wait 1 minutes
  digitalWrite( led4, LOW);
  digitalWrite( led3, HIGH);

  msec = calculDelay();
  delay(msec);              // wait 1 minutes
  digitalWrite( led3, LOW);
  digitalWrite( led2, HIGH);

  msec = calculDelay();
  delay(msec);              // wait 1 minutes
  digitalWrite( led2, LOW);
  digitalWrite( led1, HIGH);

  msec = calculDelay();
  delay(msec);              // wait 1 minutes
  digitalWrite( led1, LOW);
 
  digitalWrite(shutter, HIGH);   // take picture
  digitalWrite(led, HIGH);       // light
  delay(1000);                   // wait 1 second

  
}
