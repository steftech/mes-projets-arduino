/*
  Time Lapse
  Press Shutter for 1 second, wait for 5 minutes
 
  This code is in the public domain.
 */

int led=13; 
int shutter=8;

int led5=11;
int led4=10;
int led3=9;
// Maximum 30 minutes
double tempsMax = 30 * 60 ;
// Minimum 5 s
double tempsMin = 5 ;
double msec=5;

void setup() {                
  // initialize the digital pin as an output.
  pinMode(led, OUTPUT);
  pinMode(shutter, OUTPUT);     
  
  pinMode(led5, OUTPUT);
  pinMode(led4, OUTPUT);
  pinMode(led3, OUTPUT);
  Serial.begin(9600);
  
}

double calculDelay()
{
  int sensorValue = analogRead(5);
  Serial.print( sensorValue);
  double tempo = 0;
  tempo = (((tempsMax - tempsMin) *  sensorValue) / 200);
  tempo += tempsMin;
  Serial.print( " : ");
  Serial.println( tempo );
  tempo *= 1000;

  tempo /= 3;

  return tempo;
}

void loop() {
  
  msec = calculDelay();

  digitalWrite(shutter, LOW);    // release shutter
  digitalWrite(led, LOW);        // light off
  
  digitalWrite( led5, HIGH);
  digitalWrite( led4, LOW);
  digitalWrite( led3, LOW);
  
  msec = calculDelay();
  delay(msec);              // wait 1 minutes
  digitalWrite( led5, LOW);
  digitalWrite( led4, HIGH);

  msec = calculDelay();
  delay(msec);              // wait 1 minutes
  digitalWrite( led4, LOW);
  digitalWrite( led3, HIGH);

  msec = calculDelay();
  delay(msec);              // wait 1 minutes
  digitalWrite( led3, LOW);
  digitalWrite(shutter, HIGH);   // take picture
  digitalWrite(led, HIGH);       // light
  delay(1000);                   // wait 1 second

  
}
