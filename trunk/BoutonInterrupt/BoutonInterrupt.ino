
int nbOpen = 0;
int nbClose = 0;

int oldNbOpen =0;
int oldNbClose =0;

const int buttonPin = 2;

int last_button_time = 0;
int last_button_time_open = 0;
int last_button_time_close = 0;

void tick()
{
  int button_time = millis();
  
  int buttonState = digitalRead(buttonPin);
  if (button_time - last_button_time > 20)
  {

    if (buttonState == LOW) 
    {
      Serial.println( "open");    
      nbOpen ++;
    } 
    else 
    {
      Serial.println( "close");
      nbClose ++;
    }
    last_button_time = button_time;
  }
}

void setup()
{
  Serial.begin( 115200 );

  pinMode(buttonPin, INPUT);  
  attachInterrupt( 0, tick, CHANGE );

}

void loop()
{
  if ( nbOpen != oldNbOpen )
  {  
    Serial.print( "change is in the air : opened "); 
    Serial.println( nbOpen );
    oldNbOpen = nbOpen;
  }
  
  if ( nbClose != oldNbClose )
  {  
    Serial.print( "change is in the air : closed "); 
    Serial.println( nbClose );
    oldNbClose = nbClose;
  }
}
