#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#include "message.h"
#include "constantes.h"
#include "net.h"
#include "client.h"

#define PRGNAME "client"
#define PRGCODE "CLI"

static void init(void)
{
	printf("Init %s\n", PRGNAME);
}

static void end(void)
{
	printf("End %s\n", PRGNAME);
}

static void app_client(const char *address, const char *name)
{
   SOCKET sock = init_connection_client(address);
   char buffer[BUF_SIZE];

   Message msg;   

   fd_set rdfs;

   /* send our name */
   /* Transformation de Message en Buffer*/
   strcpy( msg.source, name);
   strcpy( msg.subject, "client.name");
   strcpy( msg.message, name);
   
	//strcpy( buffer, name );  
	serializeMessage( buffer, &msg); 
   write_server(sock, buffer);

   while(1)
   {
      FD_ZERO(&rdfs);

      /* add STDIN_FILENO */
      FD_SET(STDIN_FILENO, &rdfs);

      /* add the socket */
      FD_SET(sock, &rdfs);

      if(select(sock + 1, &rdfs, NULL, NULL, NULL) == -1)
      {
         perror("select()");
         exit(errno);
      }

      /* something from standard input : i.e keyboard */
      if(FD_ISSET(STDIN_FILENO, &rdfs))
      {
         fgets(buffer, BUF_SIZE - 1, stdin);
         {
            char *p = NULL;
            p = strstr(buffer, "\n");
            if(p != NULL)
            {
               *p = 0;
            }
            else
            {
               /* fclean */
               buffer[BUF_SIZE - 1] = 0;
            }
         }
         
         /* buffer est a transformer en Message */
			strcpy( msg.source, name);
			strcpy( msg.subject, "client.message");
			strcpy( msg.message, buffer);
			serializeMessage( buffer, &msg );
         write_server(sock, buffer);

      }
      else if(FD_ISSET(sock, &rdfs))
      {
         int n = read_server(sock, buffer);
         /* server down */
         if(n == 0)
         {
            printf("Server disconnected !\n");
            break;
         }
         /* TODO : buffer est un Message */
#if defined DEBUG
			puts(buffer);
#endif	
			unSerializeMessage( buffer, &msg );
			printMessage( &msg );

      }
   }

   end_connection(sock);
}

static int init_connection_client(const char *address)
{
   SOCKET sock = socket(AF_INET, SOCK_STREAM, 0);
   SOCKADDR_IN sin = { 0 };
   struct hostent *hostinfo;

   if(sock == INVALID_SOCKET)
   {
      perror("socket()");
      exit(errno);
   }

   hostinfo = gethostbyname(address);
   if (hostinfo == NULL)
   {
      fprintf (stderr, "Unknown host %s.\n", address);
      exit(EXIT_FAILURE);
   }

   sin.sin_addr = *(IN_ADDR *) hostinfo->h_addr;
   sin.sin_port = htons(PORT);
   sin.sin_family = AF_INET;

   if(connect(sock,(SOCKADDR *) &sin, sizeof(SOCKADDR)) == SOCKET_ERROR)
   {
      perror("connect()");
      exit(errno);
   }

   return sock;
}

static void end_connection(int sock)
{
   closesocket(sock);
}

static int read_server(SOCKET sock, char *buffer)
{
   int n = 0;

   if((n = recv(sock, buffer, BUF_SIZE - 1, 0)) < 0)
   {
      perror("recv()");
      exit(errno);
   }

   buffer[n] = 0;

   return n;
}

static void write_server(SOCKET sock, const char *buffer)
{
   if(send(sock, buffer, strlen(buffer), 0) < 0)
   {
      perror("send()");
      exit(errno);
   }
}

int main(int argc, char **argv)
{
   if(argc < 2)
   {
      printf("Usage : %s [address] [pseudo]\n", argv[0]);
      return EXIT_FAILURE;
   }

   init();

   app_client(argv[1], argv[2]);

   end();

   return EXIT_SUCCESS;
}

