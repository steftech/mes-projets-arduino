#ifndef CLIENT_H
#define CLIENT_H

#include "constantes.h"
#include "net.h"

typedef struct
{
   SOCKET sock;
   char name[BUF_SIZE];
} Client;


static void init(  );
static void end(  );
static void app_client(const char *address, const char *name);
static int init_connection_client(const char *address);
static void end_connection(int sock);
static int read_server(SOCKET sock, char *buffer);
static void write_server(SOCKET sock, const char *buffer);
//static void write_server(SOCKET sock, Message *msg);

#endif /* guard */
