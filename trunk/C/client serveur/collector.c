#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>


#include "collector.h"
#include "message.h"
#include "constantes.h"
#include "net.h"
#include "plugins.h"
#include "sir.h"
//#include "iniparser.h"

char prgName[20 + 1] = "client";
char prgCode[3 + 1]  = "COL";

char subject[30+1] = "";

#define BAUDRATE B115200
// Suite � un probleme sur le TP-LINK WR703 un hack consistant � fermer/ouvrir le port s�rie est mis en place
//  Ce compteur est le nombre de lecture avant de r�ouvrir le port
// C'est moche et exp�rimental, on est d'accord ...
int MAX_COMPTEUR = 0;

/* Collector est une version am�lior�e du Client pouvant suivre un flux s�rie en plus */

static void init( )
{
	printf("Init %s\n", prgName);
	
}

static void end( )
{
	printf("End %s\n", prgName);
}


static unsigned int isLigneValide( char * buffer )
{
	if ( strstr( buffer, "***" ) != 0 ) { return 0; }
	if ( buffer[0] == 'D' ) { return 1; }
	if ( buffer[0] == 'W' ) { return 1; }
	
	return 0;
}

static int init_serial( char * serialPort, struct termios * oldtio )
{
	int fd = 0;
	struct termios newtio;
	/* 
	   Open modem device for reading and writing and not as controlling tty
	   because we don't want to get killed if linenoise sends CTRL-C.
	*/
	fd = open( serialPort, O_RDWR | O_NOCTTY ); 
	if (fd <0) {perror(serialPort); exit(-1); }
	      
	tcgetattr(fd, oldtio); /* save current serial port settings */
	bzero( &newtio, sizeof(newtio) ); /* clear struct for new port settings */
	      
	/* 
	   BAUDRATE: Set bps rate. You could also use cfsetispeed and cfsetospeed.
	   CRTSCTS : output hardware flow control (only used if the cable has
	   all necessary lines. See sect. 7 of Serial-HOWTO)
	   CS8     : 8n1 (8bit,no parity,1 stopbit)
	   CLOCAL  : local connection, no modem contol
	   CREAD   : enable receiving characters
	*/
	// has to fit the configuration used on the avr of course
	newtio.c_cflag = BAUDRATE | CRTSCTS | CS8 | CLOCAL | CREAD;
	       
	/*
	  IGNPAR  : ignore bytes with parity errors
	  ICRNL   : map CR to NL (otherwise a CR input on the other computer
	  will not terminate input)
	  otherwise make device raw (no other input processing)
	*/
	//newtio.c_iflag = IGNPAR | ICRNL;
	newtio.c_iflag = 0;
	       
	/*
	  Raw output.
	*/
	newtio.c_oflag = 0;
	       
	/*
	  ICANON  : enable canonical input
	  disable all echo functionality, and don't send signals to calling program
	*/
	newtio.c_lflag = ICANON;
	       
	/* 
	   initialize all control characters 
	   default values can be found in /usr/include/termios.h, and are given
	   in the comments, but we don't need them here
	*/
	newtio.c_cc[VINTR]    = 0;     /* Ctrl-c */ 
	newtio.c_cc[VQUIT]    = 0;     /* Ctrl-\ */
	newtio.c_cc[VERASE]   = 0;     /* del */
	newtio.c_cc[VKILL]    = 0;     /* @ */
	newtio.c_cc[VEOF]     = 4;     /* Ctrl-d */
	newtio.c_cc[VTIME]    = 0;     /* inter-character timer unused */
	newtio.c_cc[VMIN]     = 1;     /* blocking read until 1 character arrives */
	newtio.c_cc[VSWTC]    = 0;     /* '\0' */
	newtio.c_cc[VSTART]   = 0;     /* Ctrl-q */ 
	newtio.c_cc[VSTOP]    = 0;     /* Ctrl-s */
	newtio.c_cc[VSUSP]    = 0;     /* Ctrl-z */
	newtio.c_cc[VEOL]     = 0;     /* '\0' */
	newtio.c_cc[VREPRINT] = 0;     /* Ctrl-r */
	newtio.c_cc[VDISCARD] = 0;     /* Ctrl-u */
	newtio.c_cc[VWERASE]  = 0;     /* Ctrl-w */
	newtio.c_cc[VLNEXT]   = 0;     /* Ctrl-v */
	newtio.c_cc[VEOL2]    = 0;     /* '\0' */
	      
	/* 
	   now clean the modem line and activate the settings for the port
	*/
	tcflush(fd, TCIFLUSH);
	tcsetattr(fd,TCSANOW,&newtio);
	return fd;
	
}

static void app_collector(const char *address, const char *name, int fd, unsigned int isCollector, char * arduinoSubject , Plugin ** tabPlugins, int nbPluginsLoaded, char * serialPort )
{
	SOCKET sock = init_connection_client(address);
	char buffer[BUF_SIZE];
	
	Message msg;
	
	int compteur = 0;

	fd_set rdfs;
	
	/* send our name */
	/* Transformation de Message en Buffer*/
	strcpy( msg.source, name);
	strcpy( msg.subject, "client.name");
	strcpy( msg.message, name);
	
	//strcpy( buffer, name );  
	serializeMessage( buffer, &msg); 
	write_server(sock, buffer);
	
	while(1)
	{
		
		FD_ZERO(&rdfs);
		
		/* add STDIN_FILENO */
		FD_SET(STDIN_FILENO, &rdfs);

		if ( isCollector == 1 )
		{
			if ( MAX_COMPTEUR > 0 && compteur >= MAX_COMPTEUR )
			{
				// Close serial port
				printf("Fermeture du port [%s] (%d)\n", serialPort, fd);
				if ( fd > 0 ) { close(fd); }
				// open serial port
				struct termios oldtio;
				fd = init_serial( serialPort, &oldtio );
				printf("Ouverture du port [%s] (%d)\n", serialPort, fd);
				compteur = 0;
			}

			/* Ajout de la surveillance du port s�rie */
			FD_SET(fd, &rdfs);                       
		}	
		/* add the socket */
		FD_SET(sock, &rdfs);
		
		if(select(sock + 1, &rdfs, NULL, NULL, NULL) == -1)
		{
			perror("select()");
			exit(errno);
		}
		
		/* something from standard input : i.e keyboard */
		if(FD_ISSET(STDIN_FILENO, &rdfs))
		{
			/* r�ception d'un message venant de l'entr�e standard */
			fgets(buffer, BUF_SIZE - 1, stdin);
			{
				char *p = NULL;
				p = strstr(buffer, "\n");
				if(p != NULL)
				{
				   *p = 0;
				}
				else
				{
				   buffer[BUF_SIZE - 1] = 0;
				}
			}
			
			/* Message est � transformer en buffer */
			strcpy( msg.source, name);
			strcpy( msg.subject, "client.message");
			strcpy( msg.message, buffer);
			serializeMessage( buffer, &msg );
			/* Envoi du message au serveur (qui r�percutera aux clients) */
			write_server(sock, buffer);
			
		}
		else if ( isCollector == 1 && FD_ISSET(fd, &rdfs) )
		{
			/* r�ception d'un message venant du port s�rie */
			int res = read( fd, buffer, 255 ); 
			buffer[ res - 1 ] = 0;             /* set end of string, so we can printf */
			
			// On supprime les \r, l'Arduino semble en etre friand :(
			char * pchaine = strchr( buffer, '\r');
			if ( pchaine != NULL ) { *pchaine = '\0'; }
			
			/* On ne va envoyer le message que si la ligne est "valide" */
			if ( isLigneValide( buffer ) == 1 )
			{
				compteur ++;
				/* buffer est � transformer en Message */
				strcpy( msg.source, name);
				strcpy( msg.subject, arduinoSubject);
				strcpy( msg.message, buffer);			
		
				serializeMessage( buffer, &msg );
				printMessage( &msg );
				write_server(sock, buffer);			
			}
		}
		else if(FD_ISSET(sock, &rdfs))
		{
			/* r�ception d'un message venant du r�seau */
			int n = read_server(sock, buffer);
			/* server down */
			if(n == 0)
			{
				printf("Server disconnected !\n");
				break;
			}
			/* buffer est a transformer en Message */
			#if defined DEBUG
				printf("%s\n", buffer);
			#endif	
			unSerializeMessage( buffer, &msg );
			printMessage( &msg );
			int i;
			for ( i = 0; i < nbPluginsLoaded; i++)
			{
				if ( strcmp( tabPlugins[i]->subject, msg.subject ) == 0)
				{
#ifdef DEBUG
					printf("appel du plugin[%d] [%s]\n",i, tabPlugins[i]->subject);
					printf("appel du plugin[%d] [%s]\n",i,  tabPlugins[i]->name);
#endif

					tabPlugins[i]->parse( &msg );
				}
			}
			
			
			if ( isCollector == 1 && strcmp ( msg.subject, arduinoSubject) )
			{
				/* Ecrire le message sur le port s�rie pour l'Arduino */
				//  Ecriture dans le cas o� le message est destin� � cet arduino !
				write(fd, msg.message, strlen(msg.message) );
			}
		}
	}
	
	end_connection(sock);
}

static int init_connection_client(const char *address)
{
   SOCKET sock = socket(AF_INET, SOCK_STREAM, 0);
   SOCKADDR_IN sin;
   struct hostent *hostinfo;

   if(sock == INVALID_SOCKET)
   {
      perror("socket()");
      exit(errno);
   }

   hostinfo = gethostbyname(address);
   if (hostinfo == NULL)
   {
      fprintf (stderr, "Unknown host %s.\n", address);
      exit(EXIT_FAILURE);
   }

   sin.sin_addr = *(IN_ADDR *) hostinfo->h_addr;
   sin.sin_port = htons(PORT);
   sin.sin_family = AF_INET;

   if(connect(sock,(SOCKADDR *) &sin, sizeof(SOCKADDR)) == SOCKET_ERROR)
   {
      perror("connect()");
      exit(errno);
   }

   return sock;
}

static void end_connection(int sock)
{
   closesocket(sock);
}

static int read_server(SOCKET sock, char *buffer)
{
	int n = 0;
	
	if((n = recv(sock, buffer, BUF_SIZE - 1, 0)) < 0)
	{
		perror("recv()");
		exit(errno);
	}
	
	buffer[n] = 0;
	
	return n;
}

static void write_server(SOCKET sock, const char *buffer)
{
	if(send(sock, buffer, strlen(buffer), 0) < 0)
	{
		perror("send()");
		exit(errno);
	}
}

int main(int argc, char **argv)
{
	char serialPort[255] = "";

	char arduinoSubject[255] = "";
	unsigned int isCollector = 0;
	int fd = 0;
	printf("argc : %d\n", argc);
	if(argc < 3)
	{
		printf("Usage : %s [server address] [name] <serial port> <arduino subject>\n", argv[0]);
		return EXIT_FAILURE;
	}
	
	if ( argc == 5 )
	{
		printf("Arduino Collector Activated\n");
		strncpy ( serialPort, argv[3], sizeof(serialPort) - 1 );
		serialPort[ sizeof(serialPort) - 1] = 0;
		strncpy ( arduinoSubject, argv[4], sizeof(arduinoSubject) - 1 );
		arduinoSubject[ sizeof(arduinoSubject) - 1] = 0;
		
		isCollector = 1;
		strcpy( prgName, "collector");

	}

	init( isCollector );


	Plugin ** tabPlugins = malloc ( sizeof ( Plugin * ) * MAX_PLUGINS );
	
	sirElement * firstSirElement = loadIni("collector.ini");
	sirElement * currentSirElement = firstSirElement;
	char path2Plugins[ STRING_SIZE ];
	
	int nbPluginsLoaded = 0;

	if ( firstSirElement != NULL )
	{
		sirElement * search = getElement( firstSirElement, "base", "pluginDir" );
		if ( search != NULL )
		{
			strcpy( path2Plugins, search->value);
		}				
		else
		{
			strcpy( path2Plugins, ".");
		}


		search = getElement( firstSirElement, "base", "max_compteur" );
		if ( search != NULL )
		{
			MAX_COMPTEUR = atoi( search->value);
		}				

		
		while ( currentSirElement != NULL )
		{
			if ( strcmp( currentSirElement->section, "plugins" ) == 0)
			{
				Plugin * p = (Plugin * ) malloc( sizeof( Plugin ) );
				char pluginName[255];
				sprintf( pluginName, "%s/%s", path2Plugins, currentSirElement->value);
				if ( loadPlugin( p,  pluginName) == 0 )
				{
					strcpy( p->subject, currentSirElement->key);
					if ( p->init( currentSirElement->key, firstSirElement ) )
					{
						printf("Load plugin %s Success \n", pluginName);						
						tabPlugins[ nbPluginsLoaded ] = p;
						nbPluginsLoaded ++ ;
					}
					else
					{
						printf("Init plugin %s Failed \n", pluginName);
					}
				}
				else
				{
					printf("Load plugin %s Failed \n", pluginName);
				}
				
			}
			currentSirElement = currentSirElement->next;
        } 
	}
	else
	{
		printf("No collector.ini found, no plugin activated \n");
	}
/*
	for ( i = 0 ; i < nbPluginsToLoad ; i ++ )
	{
		Plugin * p = (Plugin * ) malloc( sizeof( Plugin ) );
		if ( loadPlugin( p,  pluginsToLoad[ i ]) == 0 )
		{
			printf("Load plugin %s Success \n", pluginsToLoad[ i ]);
			p->init();
			nbPluginsLoaded ++ ;			
		}
		else
		{
			printf("Load plugin %s Failed \n", pluginsToLoad[ i ]);
		}
		tabPlugins[ i ] = p;
	}
*/
	struct termios oldtio;
	if ( isCollector == 1 )
	{
		fd = init_serial( serialPort, &oldtio );
	}
	
	app_collector(argv[1], argv[2], fd, isCollector, arduinoSubject, tabPlugins, nbPluginsLoaded, serialPort);

	if ( isCollector == 1 )
	{
	   	// Restauration des param�tres du port
		tcsetattr( fd, TCSANOW, &oldtio);	
	}
	end( isCollector );
/*	
	iniparser_freedict(ini);
*/	
	
	return EXIT_SUCCESS;
}

