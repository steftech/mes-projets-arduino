#ifndef CLIENT_H
#define CLIENT_H

#include "constantes.h"
#include "net.h"
#include "plugins.h"

static void init( );
static void end( );
static void app_collector(const char *address, const char *name, int fd, unsigned int isCollector, char * arduinoSubject , Plugin * tabPlugins[MAX_PLUGINS], int nbPluginsLoaded, char * serialPort);
static int init_connection_client(const char *address);
static void end_connection(int sock);
static int read_server(SOCKET sock, char *buffer);
static void write_server(SOCKET sock, const char *buffer);
//static void write_server(SOCKET sock, Message *msg);

#endif /* guard */
