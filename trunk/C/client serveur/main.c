#include <stdio.h>
#include <dlfcn.h>
#include <stdlib.h>
#include <string.h>



typedef int (*init_f) ();
typedef char * (*parse_f) ();

typedef struct
{
	char    fileName[ 255 ];
	char    name[255];
	char    subject[255];
	init_f  init;
	parse_f parse;
}Plugin;


int loadPlugin( Plugin * p, char * fileName )
{
	void * plugin = dlopen (fileName, RTLD_NOW);
	if (!plugin)
	{
		printf ("Cannot load %s: %s\n", fileName, dlerror ());
		return 1;
	}
	
	strcpy( p->fileName, fileName);
	 
    char * result = NULL;
	char * pPluginName = dlsym (plugin, "pluginName");
	
	result = dlerror ();
	if (result)
	{
                           
		strcpy( p->name, p->fileName);
		printf ("Cannot find pluginName in %s: %s", fileName, result);
	}
	else
	{
		strcpy( p->name, pPluginName);
	}
    
	char * pSubject = dlsym (plugin, "subject");
	result = dlerror ();
	if (result)
	{
		strcpy( p->subject, p->name);
		printf ("Cannot find pSubject in %s: %s", fileName, result);
	}
	else
	{
		strcpy( p->subject, pSubject);
	}

	init_f	init = dlsym (plugin, "init");
	result = dlerror ();
	if (result)
	{
		printf ("Cannot find init in %s: %s", pPluginName, result);
		p->init = NULL;
	}
	else
	{
		p->init = init;
	}

	
	parse_f	parse = dlsym (plugin, "parse");
	result = dlerror ();
	if (result)
	{
		p->parse = NULL;
		printf ("Cannot find parse in %s: %s", pPluginName, result);
	}
	else
	{
		p->parse = parse;
	}

	return 0;	
}

int main( )
{

	Plugin * tabPlugins[20];
	/* nombre et liste des plugins � charger, cette liste est obtenue par un fichier config ou le balayage du r�pertoire de plugins */
	int nbPluginsToLoad = 2;
	char pluginsToLoad[20][255] = { "stationMeteo.so", "plugin1.so" };
	
	int nbPluginsLoaded = 0;
    
	int i = 0;
	for ( i = 0 ; i < nbPluginsToLoad ; i ++ )
	{
		Plugin * p = (Plugin * ) malloc( sizeof( Plugin ) );
		if ( loadPlugin( p,  pluginsToLoad[ i ]) == 0 )
		{
			printf("Load plugin %s Success \n", pluginsToLoad[ i ]);
			p->init();
		}
		else
		{
			printf("Load plugin %s Failed \n", pluginsToLoad[ i ]);
		}
		tabPlugins[ i ] = p;
		nbPluginsLoaded ++ ;	
	}


	// Utilisation des plugins charg�s
	for ( i = 0 ; i < nbPluginsLoaded ; i ++ )
	{
		tabPlugins[ i ]->parse("Message de test"); 
	}


	return 0;
}
