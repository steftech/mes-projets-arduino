#ifndef MESSAGE_H
#define MESSAGE_H

#include "constantes.h"

/* un message ne doit pas faire plus de BUF_SIZE */
typedef struct
{
   char source[ 30 + 1 ];
   char subject[ 30 + 1];
   char message[ BUF_SIZE - 62 ];
} Message;


void cleanTrailingSpace( char * chaine );
void serializeMessage( char * buffer, Message * msg);
void unSerializeMessage( char * buffer, Message * msg);
void dumpMessage( Message * msg );
void printMessage( Message * msg );

#endif

