
#ifndef PLUGINS_H
#define PLUGINS_H

#include "constantes.h"
#include "sir.h"
#include <stdio.h>
#include <dlfcn.h>
#include <stdlib.h>
#include <string.h>


/* Partie de gestion des plugins */
typedef int (*init_f) ( char * _subject, sirElement * config );
typedef char * (*parse_f) ();
// TODO : plugins.h : transformer en pointeurs sur .so
typedef struct
{
	char    fileName[ 255 ];
	char    name[255];
	char    subject[255];
	init_f  init;
	parse_f parse;
} Plugin;


int loadPlugin( Plugin * p, char * fileName );
int loadPlugins( Plugin ** tabPlugins, char ** pluginsToLoad, int nbPluginsToLoad);

#endif