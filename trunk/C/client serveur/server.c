#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>


#include "server.h"
#include "message.h"
#include "constantes.h"
#include "net.h"


static void write_client(SOCKET sock, const char *buffer);
static int read_client(SOCKET sock, char *buffer);
static void end_connection(SOCKET sock);
static SOCKET init_connection(void);
static void send_message_to_all_clients(Client *clients, Client sender, int actual, const char *buffer, char from_server);
static void remove_client(Client *clients, int to_remove, int *actual);
static void clear_clients(Client *clients, int actual);
static void app(void);
static void init(void);
static void end(void);

#define PRGNAME "server"
#define PRGCODE "SER"

//#define DEBUG

static void init(void)
{
	printf("Init %s\n", PRGNAME);
}

static void end(void)
{
	printf("End %s\n", PRGNAME);
}

static void app(void)
{
	SOCKET sock = init_connection();
	char buffer[BUF_SIZE];
	Message msg;
	/* the index for the array */
	int actual = 0;
	int max = sock;
	/* an array for all clients */
	Client clients[MAX_CLIENTS];

	fd_set rdfs;
	
	while(1)
	{
		int i = 0;
		FD_ZERO(&rdfs);
		
		/* add STDIN_FILENO */
		FD_SET(STDIN_FILENO, &rdfs);
		
		/* add the connection socket */
		FD_SET(sock, &rdfs);
		
		/* add socket of each client */
		for(i = 0; i < actual; i++)
		{
			FD_SET(clients[i].sock, &rdfs);
		}
		
		if(select(max + 1, &rdfs, NULL, NULL, NULL) == -1)
		{
			perror("select()");
			exit(errno);
		}
		
		/* something from standard input : i.e keyboard */
		if(FD_ISSET(STDIN_FILENO, &rdfs))
		{
			/* stop process when type on keyboard */
			fgets(buffer, BUF_SIZE - 1, stdin);
			char *p = NULL;
			p = strchr(buffer, '\n');
			if(p != NULL)
			{
			   *p = 0;
			}
			else
			{
			   /* fclean */
			   buffer[BUF_SIZE - 1] = 0;
			}
			
			if ( strcasecmp( buffer, "quit") == 0 || strcasecmp( buffer, "exit") == 0)
			{
				break;
			}
		}
		else if(FD_ISSET(sock, &rdfs))
		
      {
         /* new client */
         SOCKADDR_IN csin = { 0 };
//         size_t sinsize = sizeof csin;
         socklen_t sinsize = sizeof csin;
         int csock = accept(sock, (SOCKADDR *)&csin, &sinsize);
         if(csock == SOCKET_ERROR)
         {
            perror("accept()");
            continue;
         }

         /* after connecting the client sends its name */
         if(read_client(csock, buffer) == -1)
         {
            /* disconnected */
            continue;
         }
			unSerializeMessage( buffer, &msg);
			printMessage( &msg );
			/* what is the new maximum fd ? */
			max = csock > max ? csock : max;
			
			FD_SET(csock, &rdfs);
			
			Client c = { csock };
			/* Normalement �a doit �tre un message de subject : client.name*/
			strncpy(c.name, msg.message, BUF_SIZE - 1);
			clients[actual] = c;
			actual++;
         
			strcpy( msg.source, "server");
			strcpy( msg.subject, "info.client.connect");
			strcpy( msg.message, c.name);
			
			serializeMessage( buffer, &msg );
			printMessage( &msg );
						
			send_message_to_all_clients(clients, c, actual, buffer, 1);
		}
		else
		{
			int i = 0;
			for(i = 0; i < actual; i++)
			{
				/* a client is talking */
				if(FD_ISSET(clients[i].sock, &rdfs))
				{
					Client client = clients[i];
					int c = read_client(clients[i].sock, buffer);
					/* buffer est un Message */
		
					#if defined DEBUG
						dumpMessage( &msg );
					#endif
					/* client disconnected */
					if(c == 0)
					{
						closesocket(clients[i].sock);
						remove_client(clients, i, &actual);
						
						strcpy( msg.source, "server");
						strcpy( msg.subject, "info.client.disconnect");
						strcpy( msg.message, client.name);
						
						serializeMessage( buffer, &msg );
						printMessage( &msg );
						
						send_message_to_all_clients(clients, client, actual, buffer, 1);
					}
					else
					{
						unSerializeMessage( buffer, &msg);
						printMessage( &msg );
						
						send_message_to_all_clients(clients, client, actual, buffer, 0);
					}
					break;
				}
			}
		}
	}

	clear_clients(clients, actual);
	end_connection(sock);
}

static void clear_clients(Client *clients, int actual)
{
	int i = 0;
	for(i = 0; i < actual; i++)
	{
		closesocket(clients[i].sock);
	}
}

static void remove_client(Client *clients, int to_remove, int *actual)
{
	/* we remove the client in the array */
	memmove(clients + to_remove, clients + to_remove + 1, (*actual - to_remove - 1) * sizeof(Client));
	/* number client - 1 */
	(*actual)--;
}

static void send_message_to_all_clients(Client *clients, Client sender, int actual, const char *buffer, char from_server)
{
	int i = 0;
	char message[BUF_SIZE];
	message[0] = 0;

	// Affichage local des infos
/*	if (from_server == 0)
	{
		printf("[%s] : %s\n", sender.name, buffer);
	}
	else
	{
		printf("[%s] : %s\n", PRGNAME, buffer);	
	}
*/   
#if defined DEBUG
	printf("[RAW SEND] : [%s]\n", buffer);
#endif

	for(i = 0; i < actual; i++)
	{
		/* we don't send message to the sender */
		if(sender.sock != clients[i].sock)
		{
/*			if(from_server == 0)
			{
				strncpy(message, sender.name, BUF_SIZE - 1);
				strncat(message, " : ", sizeof message - strlen(message) - 1);
			}
			strncat(message, buffer, sizeof message - strlen(message) - 1);
*/			
			write_client(clients[i].sock, buffer);
		}
	}
}

static SOCKET init_connection(void)
{
	SOCKET sock = socket(AF_INET, SOCK_STREAM, 0);
	SOCKADDR_IN sin = { 0 };
	
	if(sock == INVALID_SOCKET)
	{
		perror("socket()");
		exit(errno);
	}
	
	sin.sin_addr.s_addr = htonl(INADDR_ANY);
	sin.sin_port = htons(PORT);
	sin.sin_family = AF_INET;
	
/*	if(bind(sock,(SOCKADDR *) &sin, sizeof sin) == SOCKET_ERROR)
	{
		perror("bind()");
		exit(errno);
	}*/
	
	while(bind(sock,(SOCKADDR *) &sin, sizeof sin) == SOCKET_ERROR)
	{
		perror("bind()");
		//exit(errno);
		printf("Waiting a second\n");
		sleep( 1 );
	}
	printf("Bind Successfull\n");
	
	
	if(listen(sock, MAX_CLIENTS) == SOCKET_ERROR)
	{
		perror("listen()");
		exit(errno);
	}
	printf("Listen Successfull\n");
	
	return sock;
}

static void end_connection(SOCKET sock)
{
	closesocket(sock);
}

static int read_client(SOCKET sock, char *buffer)
{
	int n = 0;
	
	if((n = recv(sock, buffer, BUF_SIZE - 1, 0)) < 0)
	{
		perror("recv()");
		/* if recv error we disconnect the client */
		n = 0;
	}
	
	buffer[n] = 0;
	
	return n;
}

static void write_client(SOCKET sock, const char *buffer)
{
	if(send(sock, buffer, strlen(buffer), 0) < 0)
	{
		perror("send()");
		exit(errno);
	}
}

int main(int argc, char **argv)
{
	init();
	
	app();
	
	end();
	
	return EXIT_SUCCESS;
}
