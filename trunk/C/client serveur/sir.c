#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "sir.h"

/* SIR - Simple Ini Reader */
/* La fonction loadIni initialise une liste doublement chainée contenant chaque élément du fichier INI passé en argument */

sirElement * loadIni( char * fileName )
{
	char temp[ STRING_SIZE ];
	char * pTemp;
	char * pTempFin;
	sirElement * first = NULL;
	sirElement * current = NULL;
	
	char section[ STRING_SIZE ] = "";
	int numLigne = 1;
	
	FILE * fp = fopen ( fileName, "r" );
	if ( fp == NULL )
	{
		return NULL;
	}

	while ( fgets( temp, STRING_SIZE, fp ) != NULL )
	{
		// On nettoye la chaine en virant les commentaires (#) avant analyse
		pTemp = strchr ( temp, '#' );
		if ( pTemp != NULL ) { *pTemp = '\0'; }
		
		// On nettoye la chaine en virant ce qui pourrait être derriere un ';' avant analyse
		pTemp = strchr ( temp, '#' );
		if ( pTemp != NULL ) { *pTemp = '\0'; }

		// On nettoye la chaine en virant ce qui pourrait être derriere un '\n' avant analyse
		pTemp = strchr ( temp, '\n' );
		if ( pTemp != NULL ) { *pTemp = '\0'; }

		// On nettoye la chaine en virant ce qui pourrait être derriere un '\r' avant analyse
		pTemp = strchr ( temp, '\r' );
		if ( pTemp != NULL ) { *pTemp = '\0'; }
		
		// On cherche les chaines commençant par '[' et finissant par ']'
		pTemp = strchr ( temp, '[' );

		if ( pTemp != NULL ) 
		{ 
			pTemp++;
			pTempFin = strchr ( temp, ']' );
			if ( pTempFin != NULL ) 
			{ 
				if ( pTempFin != pTemp )
				{
					*pTempFin = '\0';
					strncpy( section, pTemp, STRING_SIZE);
				}
				else
				{
					printf("line %d : Empty section name\n", numLigne);
					return NULL;
				}
			}
			else
			{
				printf("line %d : section missing right bracket ']'\n", numLigne);
				return NULL;
			}			
			
		}


		// On recherche si on a affaire à une chaine clef=valeur
		pTemp = strchr ( temp, '=' );
		if ( pTemp != NULL ) 
		{
			char clef[ STRING_SIZE ];
			char valeur[ STRING_SIZE ];
			char * pStart;
			char * pEnd;
			pEnd = pTemp;
			*pEnd = '\0';
			pEnd --;

			pStart = temp;

			if ( pEnd != pStart )
			{
				strncpy( clef, pStart , STRING_SIZE );
			}
			else
			{
				printf("line %d : Empty key name\n", numLigne);
			}
			pTemp ++;
			strncpy( valeur, pTemp, STRING_SIZE);

			sirElement * sir = (sirElement *) malloc ( sizeof ( sirElement ) );
			sir->previous = NULL;
			sir->next = NULL;
			strcpy( sir->section, section );
			strcpy( sir->key, clef );
			strcpy( sir->value, valeur );
		
			if ( first == NULL )
			{
				first = sir;
				current = sir;				
			}
			else
			{
				current->next = sir;
				sir->previous =  current;
				current = sir;
			}
		}
		numLigne ++;
	}
	
	fclose( fp );
	return first;
	
}

void dumpVal( sirElement * element )
{
	printf("section [%s]\n", element->section );
	printf("key     [%s]\n", element->key );
	printf("value   [%s]\n", element->value );	
}

sirElement * getElement( sirElement * first, char * section, char * key )
{
	sirElement * current = first;
	while ( current != NULL )
	{
		if ( strcmp( section, current->section ) == 0 
			&& strcmp( key, current->key ) == 0 
		    )
		{
			return current;
		}
		current = current->next;
	}
	return NULL;
	
}

char * getElementValue( sirElement * search, char * defaultString )
{
	if ( search != NULL )
	{
		return search->value ;
	}
	else
	{
		return defaultString;
	}
}

unsigned char isSectionExists( sirElement * firstElement, char * section )
{
	sirElement * current = firstElement;
	while ( current != NULL )
	{
		if ( strcmp( section, current->section ) == 0 )
		{
			return 1;
		}
		current = current->next;
	}
	return 0;
}

/* Test code */
#ifdef TESTSIR

int main()
{
	sirElement * first = loadIni( "collector.ini");
	sirElement * current = first;

	while ( current != NULL )
	{
		printf("%s:%s:%s\n", current->section, current->key, current->value);
		current = current->next;
	}
	return 0;
}

#endif