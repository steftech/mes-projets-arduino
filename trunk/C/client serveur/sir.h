#ifndef SIR_H
#define SIR_H

#define STRING_SIZE 255

typedef struct sirElement
{
	struct sirElement * previous;
	char section[ STRING_SIZE ];
	char key[ STRING_SIZE ];
	char value[ STRING_SIZE ];
	struct sirElement * next;
}sirElement; 

sirElement * loadIni( char * fileName );
sirElement * getElement( sirElement * first, char * section, char * key );
char * getElementValue( sirElement * search, char * defaultString );
void dumpVal( sirElement * element );
unsigned char isSectionExists( sirElement * firstElement, char * section );
#endif