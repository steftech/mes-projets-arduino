CREATE DATABASE Arduino


CREATE TABLE  `Arduino`.`sondes` (
  `idSonde` varchar(30) NOT NULL,
  `libelleSonde` varchar(30) NOT NULL,
  PRIMARY KEY (`idSonde`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1


CREATE TABLE  `Arduino`.`mesures_sondes` (
  `dateMesure` date NOT NULL,
  `heureMesure` time NOT NULL,
  `lumiere1` smallint(5) unsigned NOT NULL,
  `lumiere2` smallint(5) unsigned NOT NULL,
  `temperature1` decimal(2,2) NOT NULL,
  `temperature2` decimal(2,2) NOT NULL,
  `temperature3` decimal(2,2) NOT NULL,
  `temperature4` decimal(2,2) NOT NULL,
  `temperature5` decimal(2,2) NOT NULL,
  PRIMARY KEY (`dateMesure`,`heureMesure`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1
