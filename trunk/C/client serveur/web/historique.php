<?php
	require_once('MySQLConnect.php');
$title = "Dernieres Mesures en diret de la station Météo";
	
	require_once("header.php");
	require_once("menu.php");
	$delta =  ( is_numeric( $_GET["delta"] ) ?  $_GET["delta"] : 0 );

?>	
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Heure');
	data.addColumn('number', 'Lum 1');
        data.addColumn('number', 'Lum 2');
        data.addColumn('number', 'Temp Chris');
        data.addColumn('number', 'Temp Enceinte');
        data.addColumn('number', 'Temp Enceinte2');
        data.addColumn('number', 'Temp Fenetre');
        data.addColumn('number', 'Temp Haut');

	
<?php
	echo "        data.addRows([";
	$sql = "select dateMesure, DATE_FORMAT(heureMesure, '%H') as heure";
	$sql .= ", avg(lumiere1) as lumiere1, avg(lumiere2) as lumiere2";
	$sql .= " , avg(temperature1) as temperature1, avg(temperature2) as temperature2
		, avg(temperature3) as temperature3, avg(temperature4) as temperature4
		, avg(temperature5) as temperature5
		from mesures_sondes
		where dateMesure=date( subdate(now(), $delta))
		group by 1, 2";
	$res = mysql_query( $sql );
	while ( $row = mysql_fetch_array( $res ) )
	{
		printf("['%02dh', %d, %d ,%.2f,%.2f,%.2f,%.2f,%.2f],\n"
			, $row{"heure"}
			, $row{"lumiere1"}
			, $row{"lumiere2"}
			, $row{"temperature1"}
			, $row{"temperature2"}
			, $row{"temperature3"}
			, $row{"temperature4"}
			, $row{"temperature5"});
	} 
?>	
        ]);

        var options = {
          title: 'Sondes'
          , series: [
		          { targetAxisIndex:1, pointSize:5}
          		, { targetAxisIndex:1, pointSize:5}
        		, { targetAxisIndex:0}
          		, { targetAxisIndex:0}
          		, { targetAxisIndex:0}
          		, { targetAxisIndex:0}
          		, { targetAxisIndex:0}
          		]
        };

        var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
    </script>
  </head>
  <body>
    <div>
	    <span ><a href="?delta=<? echo $delta + 1 ?>">&lt;</a></span>
    	    <span ><a href="?delta=<? echo $delta - 1 ?>">&gt;</a></span>

    </div>    
    <div id="chart_div" style="width: 1300px; height: 800px;"></div>

<?php    
	require_once("footer.php");
?>
