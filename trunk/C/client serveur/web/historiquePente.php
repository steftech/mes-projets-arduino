<?php
        require_once('MySQLConnect.php');
$title = "Calculs de pente sur courbe";
        
        require_once("header.php");
        require_once("menu.php");
        $delta =  ( is_numeric( @$_GET["delta"] ) ?  $_GET["delta"] : 0 );
        $donnee=  @$_GET["donnee"];
        switch($donnee )
        {
		case 'lumiere1' : $titre = "Lum 1"; break;
		case 'lumiere2' : $titre = "Lum 2"; break;
		case 'temperature1' :   $titre = 'Temp Bureau Chris';break;
		case 'temperature2' :   $titre = 'Temp Enceinte';break;
		case 'temperature3' :   $titre = 'Temp Enceinte2';break;
		case 'temperature4' :   $titre = 'Temp Fenetre';break;
		case 'temperature5' :   $titre = 'Temp Haut'; break;
		default : $donnee = 'lumiere1';$titre = "Lum 1"; break;
	}
        

?>      
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Heure');
        data.addColumn('number', '<?php echo $titre ?>');
        data.addColumn('number', 'pente');

        
<?php
        echo "        data.addRows([";
        $sql = "select dateMesure, heureMesure, TIME_TO_SEC(heureMesure) as nbSec";
        $sql .= ", $donnee as donnee 
                from mesures_sondes
                where dateMesure=date( subdate(now(), $delta))";
        $res = mysql_query( $sql );
        $oldDonnee = 0;
        $oldDate = 0;
        while ( $row = mysql_fetch_array( $res ) )
        {
        	
      		if ( $oldDate == 0 )
      		{
				$pente = 0;  
		}
		else
		{
			if ( ( $oldDonnee - $row{"donnee"} ) < 1 )
			{
				$pente = 0;
	//			$pente = ( $oldDonnee - $row{"donnee"} ) / ( $oldDate - $row{"nbSec"} );
			}
			else
			{
				$pente = ( $oldDonnee - $row{"donnee"} ) / ( $oldDate - $row{"nbSec"} );
			}
		}
      	 
		printf("['%s', %.2f, %.2f ],\n"
                   	, $row{"heureMesure"}
                    	, $row{"donnee"}
						, $pente );
			$oldDonnee = $row{"donnee"};
			$oldDate   = $row{"nbSec"};
      	} 
?>      
        ]);

        var options = {
          title: '<?php echo $titre?>'
          , series: [
                         { targetAxisIndex:1}
			,{ targetAxisIndex:0}
                        ]
        };

        var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
    </script>
  </head>
  <body>
    <div>
            <span ><a href="?delta=<? echo $delta + 1 ?>&donnee=<? echo $donnee ?>">&lt;</a></span>
            <span ><a href="?delta=<? echo $delta - 1 ?>&donnee=<? echo $donnee ?>">&gt;</a></span>

    </div>    
    <div id="chart_div" style="width: 1300px; height: 800px;"></div>

<?php    
        require_once("footer.php");
?>

