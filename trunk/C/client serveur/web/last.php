<?php
	require_once('MySQLConnect.php');
$title = "Dernieres Mesures en diret de la station M�t�o";
	
	require_once("header.php");
	require_once("menu.php");
	
	function box( $titre, $content)
	{
		echo "<span class=box>\n";
		printf("  <div class=boxTitle>%s</div>\n", $titre);
		printf("  <div class=boxContent>%s</div>\n", $content);
		echo "</span>\n";
	
	}

	$sql = "SELECT * FROM mesures_sondes ORDER BY dateMesure DESC , heureMesure DESC LIMIT 1";
	$res = mysql_query( $sql );
	if ( $row = mysql_fetch_array( $res ) )
	{
		box( "date derniere Mesure", $row{"dateMesure"} );
		box( "heure derniere Mesure", $row{"heureMesure"} );
		
		box( "luminosit� 1", $row{"lumiere1"} );
		box( "luminosit� 2", $row{"lumiere2"} );

		box( "temp Chris", $row{"temperature1"}." �C" );
		box( "temp Enceinte", $row{"temperature2"}." �C" );
		box( "temp Enceinte2", $row{"temperature3"}." �C" );
		box( "temp Fenetre", $row{"temperature4"}." �C" );
		box( "temp Haut", $row{"temperature5"}." �C" );
	} 
		
	require_once("footer.php");
?>
