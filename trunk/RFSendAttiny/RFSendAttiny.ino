/*
  Example for different sending methods
  
  http://code.google.com/p/rc-switch/
  
  Need help? http://forum.ardumote.com
*/

#include <RCSwitch.h>

RCSwitch mySwitch = RCSwitch();

int pin[4] = {1,2,3,4};
char dip[ 5 + 1 ] = "00010";

void setup() {

  
  // Transmitter is connected to Arduino Pin #10  
  mySwitch.enableTransmit(0);

  // Optional set pulse length.
  // mySwitch.setPulseLength(320);
  
  // Optional set protocol (default is 1, will work for most outlets)
  // mySwitch.setProtocol(2);
  
  // Optional set number of transmission repetitions.
  // mySwitch.setRepeatTransmit(15);
  
  for ( int i = 0; i < 4; i++)
  {
    pinMode(pin[i], INPUT);
    int buttonState = digitalRead( pin[i] );
    dip[ 4 - i ] = (buttonState == HIGH ? '1' : '0');
  }
  dip[ 4 ] = '\0';
}

void loop() {
  
  
  /* See Example: TypeA_WithDIPSwitches */
  mySwitch.switchOn("11111", dip);
  delay(1000);
  mySwitch.switchOff("11111", dip);
  delay(1000);

  delay(20000);
}
