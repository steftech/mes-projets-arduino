
int buttons[4] = {1,2,3,4};

unsigned long tableDelais[ 16 ] = { 1000,2000,5000,10000,15000, 30000, 45000,60000,90000,120000,240000, 300000,450000,600000,900000,1800000 };
unsigned long waitingTime = tableDelais[0];

int trigger = 0;
int triggerTime = 200;

void setup()
{
  // make the pushbutton's pin an input:
  for (int i=0; i < 4; i++)
  {
    pinMode( buttons[ i ] , INPUT);
  }
  int index = (digitalRead( buttons[3] ) ? 0 : 8 )
                    + (digitalRead( buttons[2] ) ? 0 : 4 )
                    + (digitalRead( buttons[1] ) ? 0 : 2 )
                    + (digitalRead( buttons[0] ) ? 0 : 1 );

  waitingTime = tableDelais[ index ];
  
  pinMode( trigger , OUTPUT);
  
}

void loop()
{
  digitalWrite( trigger, HIGH);
  delay( triggerTime );  
  digitalWrite( trigger, LOW);
  
  delay( waitingTime  );        // delay in between reads for stability

                    
}
