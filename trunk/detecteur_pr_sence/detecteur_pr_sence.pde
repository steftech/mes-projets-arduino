
int ledPin = 13;
int switchPin = 2;
int value = 0;

void setup() 
{
  pinMode(ledPin, OUTPUT);
  pinMode(switchPin, INPUT);
//  Serial.begin(115200);
}

void loop() 
{
  value = digitalRead(switchPin);
  if (HIGH == value) 
  {
    digitalWrite(ledPin, HIGH);
//    Serial.println( "HIGH");
  } 
  else 
  {
    digitalWrite(ledPin, LOW);
  }
  //delay(1000);
}
