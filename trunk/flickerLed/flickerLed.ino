/*
  Blink
  Turns on an LED on for one second, then off for one second, repeatedly.
 
  This example code is in the public domain.
 */
 
// Pin 13 has an LED connected on most Arduino boards.
// give it a name:
int led = 1;
int max_count_Normal = 60; 
int max_count_Flicker = 64;

byte Norm_Glow_table[] = {   10, 10, 10, 10, 20, 30, 30, 30, 40, 50, 60, 70, 80, 90,100, // The table for our normal glowing
                            110,120,130,140,150,160,170,180,190,200,
                            210,220,230,240,250,250,250,250,250,250,
                            240,230,220,210,200,190,180,170,160,150,
                            140,130,120,110,100, 90, 80, 70, 60, 50,
                             40, 30, 30, 30, 20, 10, 10, 10, 10 };

byte Flicker_table[] = {      10, 10, 20, 30, 30, 30, 40, 50, 60, 70, 80, 70, 70,     // the table that tells us how to flicker
                            60, 60, 50, 50, 50, 60, 70, 80, 90, 100,
                            120,140,160,240,250,100,150,250,250,140,
                            240,230,220,100, 80,70,70, 70, 80, 80,
                            140,130,120,110,200,210,220,220,100, 90,
                             40, 30, 30, 30, 20, 10, 10 };
                             
static uint8_t lcg(void) 
{
        static uint8_t state;
        state = (5 * state) + 129;
        return state;
} 

// the setup routine runs once when you press reset:
void setup() 
{                
  // initialize the digital pin as an output.
  pinMode(led, OUTPUT);     
}

// the loop routine runs over and over again forever:
void loop() {
  for (int count=0; count <= max_count_Flicker; count ++ )
  {
    analogWrite(led, Flicker_table[count]);  // Runs through the Norm_Glow_table 
    count++;                                         // We stay in this loop till the switch is activaled
    delay(40);  
  }
  for (int count=0; count <= max_count_Normal; count ++ )
  {
    analogWrite(led, Norm_Glow_table[count]);  // Runs through the Norm_Glow_table 
    count++;                                         // We stay in this loop till the switch is activaled
    delay(40);  
  }  
  for (int count=0; count <= max_count_Flicker; count ++ )
  {
    analogWrite(led, Flicker_table[count]);  // Runs through the Norm_Glow_table 
    count++;                                         // We stay in this loop till the switch is activaled
    delay(40);  
  }
  
/*  uint8_t next = lcg();
  if ( next > 90 ) 
  { digitalWrite(led, HIGH); }
  else
  {  digitalWrite(led, LOW); }
  delay(80);*/
}
