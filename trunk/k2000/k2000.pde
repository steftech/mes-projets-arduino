/*
  K200 is Back
  This code is in the public domain.
 */

int leds[8] = { 2,3,4,5,6,7,8,9  };
int nbleds = 8;
int delais = 80;

void switchOn( int pos ) 
{ 
  digitalWrite( leds[ pos], HIGH);
}

void switchOff( int pos ) 
{ 
  digitalWrite( leds[ pos], LOW);
}

void wait() { delay( delais );  }

void setup()
{  
  Serial.begin(9600);
  for (int i = 0; i < ( nbleds); i++)
  {
    pinMode(leds[i], OUTPUT);
  }
}

void loop()
{

  int position = 0;
  int oldPosition = 0;
  int direction = 1; // 1 => gauche - droite ie sens d'incrementation
                     // -1 => droite - gauche ie le sens de décrementation

  int compteur = 1;
  switchOn( position );
  wait();
  while ( compteur > 0 )
  {
    if ( position == 0 ) { direction = 1; }
    if ( position == (nbleds - 1) ) { direction = -1; }
    position += direction;
    switchOff( oldPosition );
    switchOn ( position);
    wait();
    oldPosition = position;
  }

}
