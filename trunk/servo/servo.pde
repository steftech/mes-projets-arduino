#include <Servo.h> 
 
Servo myservo;  // create servo object to control a servo 
                // a maximum of eight servo objects can be created 

Servo myservo2;  // create servo object to control a servo 

 
int pos = 0;    // variable to store the servo position 

// nombre de ms pour parcourrir un degré d'angle
// 0.12s pour 60° à 4.8V
float vitesse_angulaire = 2;
int angle = 0; 

char buffer[20];

//int angleMin = 12;
//int angleMax = 145;
int angleMin = 5;
int angleMax = 360;


//int ReceptionNombre=0; // variable de calcul du nombre reçu par port série
void setup() 
{ 
  myservo.attach(7);  // attaches the servo on pin 9 to the servo object 
  myservo2.attach(8);  // attaches the servo on pin 9 to the servo object 
  Serial.begin(115200);  
  myservo.write(90);
  myservo2.write(90);
} 

int octetReception=0; // variable de stockage des valeurs reçues sur le port Série
long nombreReception=0; // variable de stockage du nombre  reçu sur le port Série
long nombreReception0=0; // variable de stockage du dernier nombre  reçu sur le port Série
String chaineReception=""; // déclare un objet String vide pour reception chaine
 
 
void loop() 
{ 

  if (Serial.available()>0) 
  { // si caractère dans la file d'attente

//    while (Serial.available()>0) 
/*    while (1) 
    { // tant qu'un octet en réception 
      octetReception=Serial.read(); // Lit le 1er octet reçu et le met dans la variable

      if (octetReception==10) { // si Octet reçu est le saut de ligne 
                break; // sort de la boucle while
      }
      else { // si le caractère reçu n'est pas un saut de ligne
                chaineReception=chaineReception+char(octetReception); // ajoute le caratère au String
      }

    } // fin tant que  octet réception
*/ 
    chaineReception= "";
    while ( (octetReception=Serial.read() ) != 10) 
    { // tant qu'un octet en réception 
      chaineReception=chaineReception+char(octetReception); // ajoute le caratère au String
      
    } 
  
    int myStringLength = chaineReception.length()+1;
  
    char myChar[myStringLength];
    chaineReception.toCharArray(myChar,myStringLength);
    Serial.print ("Chaine recu= ");
    Serial.println(chaineReception);
    Serial.println( myChar ); 
    char * pch = strstr( myChar, ";");
    if ( pch != NULL)
    {
      *pch = '\0';
      pch++;
      Serial.print( "pch : " );
      Serial.println( pch );
      
    }
    Serial.print( "myChar : " );
    Serial.println( myChar ); 
    int ReceptionNombre = 0;
    ReceptionNombre = atoi( myChar );  
    
    if ( ReceptionNombre > angleMax ) { angle = angleMax; }
    else 
    {
      if ( ReceptionNombre < angleMin ) { angle = angleMin; }
      else { angle = ReceptionNombre; }
    }
    myservo2.write(angle);              // tell servo to go to position in variable 'pos' 
    Serial.print( "angle main :" );
    Serial.println( (int)angle );


    ReceptionNombre = 0;
    if ( pch  != NULL )
    {
      ReceptionNombre = atoi( pch );  
      if ( ReceptionNombre > angleMax ) { angle = angleMax; }
      else 
      {
        if ( ReceptionNombre < angleMin ) { angle = angleMin; }
        else { angle = ReceptionNombre; }
      }
      Serial.print( "angle bras :" );
      Serial.println( (int)angle );
      myservo.write(angle);              // tell servo to go to position in variable 'pos' 
    }
    
    chaineReception = "";

//    delay(angle * vitesse_angulaire);
  }
} 
